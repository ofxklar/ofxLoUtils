#include "ofxLoDelayParticles.h"

DelayParticles::DelayParticles(){

}

void DelayParticles::update(){
  std::vector<RectangleParticle> ::iterator iter5;
  for(iter5 = rectangleParticleList.begin(); iter5 != rectangleParticleList.end();)   {
    if ( !(iter5)->isAlive() ){
      iter5 = rectangleParticleList.erase(iter5);
    }else{
      (iter5)->update();
      ++ iter5;
    } 
  }

  std::vector<SquareParticle> ::iterator iter4;
  for(iter4 = squareParticleList.begin(); iter4 != squareParticleList.end();)   {
    if ( !(iter4)->isAlive() ){
      iter4 = squareParticleList.erase(iter4);
    }else{
      (iter4)->update();
      ++ iter4;
    } 
  }


  std::vector<CircleParticle> ::iterator iter3;
  for(iter3 = circleParticleList.begin(); iter3 != circleParticleList.end();)   {
    if ( !(iter3)->isAlive() ){
      iter3 = circleParticleList.erase(iter3);
    }else{
      (iter3)->update();
      ++ iter3;
    } 
  }

  std::vector<SimpleParticle> ::iterator iter2;
  for(iter2 = particleList.begin(); iter2 != particleList.end();) {
    if ( !(iter2)->isAlive() ){
      iter2 = particleList.erase(iter2);
    }else{
      (iter2)->update();
      ++ iter2;
    } 
  }

  std::vector<LineParticle> ::iterator iter;
  for(iter = lineParticleList.begin(); iter != lineParticleList.end();) {
    if ( !(iter)->isAlive() ){
      iter = lineParticleList.erase(iter);
    }else{
      (iter)->update();
      ++ iter;
    } 
  }
}

void DelayParticles::draw(){

  std::vector<CircleParticle> ::iterator iter3;
  for(iter3 = circleParticleList.begin(); iter3 != circleParticleList.end();++iter3) {
    (iter3)->draw();
  }

  std::vector<LineParticle> ::iterator iter2;
  for(iter2 = lineParticleList.begin(); iter2 != lineParticleList.end();++iter2) {
    (iter2)->draw();
  }

  std::vector<SimpleParticle> ::iterator iter;
  for(iter = particleList.begin(); iter != particleList.end();++iter) {
    (iter)->draw();
  }

  std::vector<SquareParticle> ::iterator iter4;
  for(iter4 = squareParticleList.begin(); iter4 != squareParticleList.end();++iter4) {
    (iter4)->draw();
  }

  std::vector<RectangleParticle> ::iterator iter5;
  for(iter5 = rectangleParticleList.begin(); iter5 != rectangleParticleList.end();++iter5) {
    (iter5)->draw();
  }

}

void DelayParticles::setLifespan(int life_in){
  lifespan = life_in;
}

void DelayParticles::addParticle(ofPoint pos, ofPoint vit, int size, int col, float sat, ofPoint accel){
  SimpleParticle newParticle(pos, vit, size);
 
  int r, v, b;

  hsv2rgb(col, r, v, b);
  r = r + (128-r)*(1-sat);
  v = v + (128-v)*(1-sat);
  b = b + (128-b)*(1-sat);
  newParticle.setColor(r, v, b, sat*255);
  newParticle.setAccel(accel);
  newParticle.setLifespan(lifespan);
  particleList.push_back(newParticle);
}

void DelayParticles::addLineParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size, ofColor col, ofPoint accel){
  LineParticle newParticle(pt1, pt2, vit, size);

  newParticle.setColor(col);
  newParticle.setAccel(accel);
  newParticle.setLifespan(lifespan);
  lineParticleList.push_back(newParticle);
}

void DelayParticles::addCircleParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size, ofColor col, ofPoint accel){
  CircleParticle newParticle(pt1, pt2, vit, size);

  newParticle.setColor(col);
  newParticle.setAccel(accel);
  newParticle.setLifespan(lifespan);
  circleParticleList.push_back(newParticle);
}

void DelayParticles::addCircleParticle(CircleParticle new_particle){
  circleParticleList.push_back(new_particle);
}


void DelayParticles::addSquareParticle(ofPoint pos, 
				       ofPoint vit, 
				       int size, 
				       int col, 
				       float sat, 
				       ofPoint accel){

  SquareParticle newParticle(pos, vit, size); 
  int r, v, b;

  hsv2rgb(col, r, v, b);
  r = r + (128-r)*(1-sat);
  v = v + (128-v)*(1-sat);
  b = b + (128-b)*(1-sat);
  newParticle.setColor(r, v, b, sat*255);
  newParticle.setAccel(accel);
  newParticle.setLifespan(lifespan);
  squareParticleList.push_back(newParticle);
}

void DelayParticles::addRectangleParticle(ofPoint pos, 
					  ofPoint vit, 
					  int size, 
					  int width,
					  int col, 
					  float sat, 
					  ofPoint accel){

  RectangleParticle newParticle(pos, vit, size, width); 
  int r, v, b;

  hsv2rgb(col, r, v, b);
  r = r + (128-r)*(1-sat);
  v = v + (128-v)*(1-sat);
  b = b + (128-b)*(1-sat);
  newParticle.setColor(r, v, b, sat*255);
  newParticle.setAccel(accel);
  newParticle.setLifespan(lifespan);
  rectangleParticleList.push_back(newParticle);
}

void DelayParticles::addRectangleParticle(RectangleParticle new_particle){
  rectangleParticleList.push_back(new_particle);
}

DelayParticles::~DelayParticles(){

}
