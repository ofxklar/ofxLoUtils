#ifndef SQUARE_PARTICLE_H
#define SQUARE_PARTICLE_H

#include "ofxLoSimpleParticle.h"

class SquareParticle : public SimpleParticle
{
 public:
  SquareParticle(ofPoint position, ofPoint vit, int size);
  virtual ~SquareParticle();

  void draw();

 protected:
  float angle;
};

#endif
