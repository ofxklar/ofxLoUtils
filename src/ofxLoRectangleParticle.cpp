#include "ofxLoRectangleParticle.h"

RectangleParticle::RectangleParticle(ofPoint position, ofPoint vitesse, int size_in, int width_in) : SimpleParticle(position, vitesse, size_in)
{
  width = width_in;
  accel.set(0,0);
  angle = atan(vitesse.y/vitesse.x) * 180 / 3.14;
}

void RectangleParticle::draw(){

  ofPushStyle();
  ofPushMatrix();
  ofTranslate(pos.x, pos.y);
  ofRotate(angle);
  ofSetColor(color);
  ofFill();
  ofRect(-width/2, -size/2, width, size);
  // ofNoFill();
  // ofSetColor(color);
  // ofRect(-width/2, -size/2, width, size);
  ofPopMatrix();
  ofPopStyle();

}

RectangleParticle::~RectangleParticle(){
  
}
