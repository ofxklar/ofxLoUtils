#include "ofxLoUtils.h"

ofPoint changeRef(ofPoint xy, MonitorRef ref){
  ofPoint xy_out;
  xy_out.set((xy.x - ref.pos_x) / ref.ratio_x,
	     (xy.y - ref.pos_y) / ref.ratio_y);
  return xy_out;
}

ofColor noteToColorFifth(float midi_note){
  int midi_note_int = round(midi_note);
  int reste = midi_note_int%12;
  int num_fifth;
  ofColor color;
  switch(reste){
    case 0:
      // Do
      num_fifth = 0;
      break;
    case 7:
      // Sol
      num_fifth = 1;
      break;
    case 2:
      // Re
      num_fifth = 2;
      break;
    case 9:
      // La
      num_fifth = 3;
      break;
    case 4:
      // Mi
      num_fifth = 4;
      break;
    case 11:
      // Si
      num_fifth = 5;
      break;
    case 6:
      // Fa#
      num_fifth = 6;
      break;
    case 1:
      // Reb
      num_fifth = 7;
      break;
    case 8:
      // Lab
      num_fifth = 8;
      break;
    case 3:
      // Mib
      num_fifth = 9;
      break;
    case 10:
      // Sib
      num_fifth = 10;
      break;
    case 5:
      // Fa
      num_fifth = 11;
      break;
  }
  color.setHsb(ofMap(num_fifth, 0, 12, 0, 256), 256, 256);
  return color; 
}

void hsv2rgb(int h, int & r, int & g, int & b){
  int H = h/60;
  float hh = h/60.-H;

  int resul[3];
  switch(H){
  case 0:
    r = 255;
    g = hh*255;
    b = 0;
    break;
  case 1:
    r = 255-255*hh;
    g = 255;
    b = 0;
    break;
  case 2:
    r = 0;
    g = 255;
    b = hh*255;
    break;
  case 3:
    r = 0;
    g = 255-hh*255;
    b = 255;
    break;
  case 4:
    r = hh*255;
    g = 0;
    b = 255;
    break;
  case 5:
    r = 255;
    g = 0;
    b = 255-hh*255;
    break;
  }

  r = max(0, min(r, 255));
  g = max(0, min(g, 255));
  b = max(0, min(b, 255));

}
