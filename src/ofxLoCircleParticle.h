#ifndef CIRCLE_PARTICLE_H
#define CIRCLE_PARTICLE_H

#include "ofxLoSimpleParticle.h"

class CircleParticle : public SimpleParticle
{
 public:
  CircleParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size);
  virtual ~CircleParticle();
  
  void update();
  void draw();

 protected:
  ofPoint pt1;
  ofPoint pt2;
};

#endif
