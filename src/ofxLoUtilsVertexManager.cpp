#include "ofxLoUtilsVertexManager.h"

VertexManager::VertexManager(){
  ofAddListener(ofEvents().mousePressed, this, &VertexManager::mousePressed);
  ofAddListener(ofEvents().mouseDragged, this, &VertexManager::mouseDragged);
  ofAddListener(ofEvents().mouseReleased, this, &VertexManager::mouseReleased);
}

//--------------------------------------------------------------------------------
void VertexManager::update(){
  
}

//--------------------------------------------------------------------------------
void VertexManager::draw(){
  std::vector<DraggableVertex> ::iterator iter;
  for(iter = verticesList.begin(); iter != verticesList.end();++iter) {
    (iter)->draw();
  }
}

//--------------------------------------------------------------------------------
void VertexManager::addVertex(ofPoint pos, int radius, ofColor color){
  DraggableVertex newVertex(pos, radius, color);
  verticesList.push_back(newVertex);
}

void VertexManager::addVertex(DraggableVertex vertex){
  verticesList.push_back(vertex);
}

bool VertexManager::isOver(int id, int x, int y){
  return verticesList[id].isOver(x, y);
}

void VertexManager::removeVertex(int id){
  verticesList.erase(verticesList.begin() + id);
}

void VertexManager::desactivateVertex(int id){
  verticesList[id].desactivate();
}

//--------------------------------------------------------------------------------
void VertexManager::mousePressed(ofMouseEventArgs &e){
  std::vector<DraggableVertex> ::iterator iter;
  for(iter = verticesList.begin(); iter != verticesList.end();++iter) {
    (iter)->mousePressed(e.x, e.y, e.button);
  }
}

//--------------------------------------------------------------------------------
void VertexManager::mouseDragged(ofMouseEventArgs &e){
  std::vector<DraggableVertex> ::iterator iter;
  for(iter = verticesList.begin(); iter != verticesList.end();++iter) {
    (iter)->mouseDragged(e.x, e.y, e.button);
  }

}

//--------------------------------------------------------------------------------
void VertexManager::mouseReleased(ofMouseEventArgs &e){
  std::vector<DraggableVertex> ::iterator iter;
  for(iter = verticesList.begin(); iter != verticesList.end();++iter) {
    (iter)->mouseReleased(e.x, e.y, e.button);
  }
}

//--------------------------------------------------------------------------------


void VertexManager::setVertexOpacity(int id, float opacity){
  verticesList[id].setOpacity(opacity);
}

DraggableVertex VertexManager::getVertex(int id){
  return verticesList[id];
}

ofPoint VertexManager::getVertexPosition(int id){
  return verticesList[id].getPosition();
}

int VertexManager::getVertexSize(int id){
  return verticesList[id].getSize();
}

ofColor VertexManager::getVertexColor(int id){
  return verticesList[id].getColor();
}


int VertexManager::getLength(){
  return verticesList.size();
}

//--------------------------------------------------------------------------------
VertexManager::~VertexManager(){
  
}
