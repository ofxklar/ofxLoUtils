#include "ofxLoUtilsVertex.h"

DraggableVertex::DraggableVertex(ofPoint pos_in, int radius_in, ofColor color_in){
  pt = pos_in;
  radius = radius_in;
  color = color_in;
  color_contour = color_in;
  bFill = true;
  bBeingDragged = false;
  line_width = 1;
  opacity = 1;
  bOver = false;
  bActive = true; 
  b_monitor = false;
  track = 0;
  type = "default";
}

//--------------------------------------------------------------------------------
void DraggableVertex::draw(){
  if (bActive){
    ofPushStyle();
    ofSetColor(color, opacity*254);
    if (bFill){
      ofFill();
      ofCircle(pt, radius);

      if (opacity != 0){
	    ofSetColor(color);
	    ofCircle(pt, radius * opacity);
      }
    }else{
      ofNoFill();
      ofSetLineWidth(line_width);
      ofSetColor(color_contour);
      ofCircle(pt, radius);
    }
    

    if (type == "default"){
      ofSetColor(0);
    }else{
      ofSetColor(255);
    }
    ofNoFill();
    ofSetLineWidth(2);
    for (int i = 0; i < track+1; i++){
      //ofCircle(pt, i*radius / (track+1));
      ofCircle(pt, radius - i*6);
    }
    ofPopStyle();
  }
}

//--------------------------------------------------------------------------------
void DraggableVertex::setMonitorRef(MonitorRef monitor_in){
  monitor = monitor_in;
  b_monitor = true;
}

void DraggableVertex::setColor(ofColor col_in){
  color = col_in;
}

void DraggableVertex::setOpacity(float opacity_in){
  opacity = opacity_in;
}

void DraggableVertex::setContour(ofColor col_cont, int line_width_in){
  bFill = false;
  line_width = line_width_in;
  color_contour = col_cont;
}

void DraggableVertex::setTrack(int track_in){
  track = track_in;
}

void DraggableVertex::setType(string type_in){
  type = type_in;
}

ofPoint DraggableVertex::getPosition(){
  return pt;
}

int DraggableVertex::getSize(){
  return radius;
}

ofColor DraggableVertex::getColor(){
  return color;
}

int DraggableVertex::getTrack(){
  return track;
}

float DraggableVertex::getRadius(){
  return radius;
}

void DraggableVertex::desactivate(){
  bActive = false;
}

bool DraggableVertex::isActive(){
  return bActive;
}

bool DraggableVertex::isOver(int x, int y){
  bool is_over;
  ofPoint xy(x, y);
  float dist_max = radius;
  if (b_monitor){
    xy = changeRef(ofPoint(x, y), monitor);
    dist_max /= monitor.ratio_x;
  }

  float diffx = xy.x - pt.x;
  float diffy = xy.y - pt.y;
  float dist = sqrt(diffx*diffx + diffy*diffy);
  if (dist < dist_max){
    is_over = true;
  } else {
    is_over = false;
  }	  

  return is_over;
}

//--------------------------------------------------------------------------------
void DraggableVertex::mouseDragged(int x, int y, int button){
  if (bActive){
    ofPoint xy(x,y);
    float dist_max = radius;
    if (b_monitor){
      xy = changeRef(ofPoint(x, y), monitor);
      dist_max /= monitor.ratio_x;
    }
    
    if (bBeingDragged == true){
      pt.x = xy.x;
      pt.y = xy.y;
    } 
  }
} 

//--------------------------------------------------------------------------------
void DraggableVertex::mousePressed(int x, int y, int button){
  if (bActive){
    ofPoint xy(x,y);
    float dist_max = radius;
    if (b_monitor){
      xy = changeRef(ofPoint(x, y), monitor);
      dist_max /= monitor.ratio_x;
    }
    
    float diffx = xy.x - pt.x;
    float diffy = xy.y - pt.y;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < dist_max){
      bBeingDragged = true;
    } else {
      bBeingDragged = false;
    }	
  }
}

//--------------------------------------------------------------------------------
void DraggableVertex::mouseReleased(int x, int y, int button){
  if (bActive){
    bBeingDragged = false;
  }
}

//--------------------------------------------------------------------------------
DraggableVertex::~DraggableVertex(){
  
}
