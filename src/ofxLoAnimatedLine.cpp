#include "ofxLoAnimatedLine.h"

AnimatedLine::AnimatedLine(){
  color = ofColor(255);
  lifespan = 30000;
}

void AnimatedLine::setColor(ofColor col_in){
  color = col_in;
}

void AnimatedLine::setLifespan(int life_in){
  lifespan = life_in;
}

void AnimatedLine::update(){
  std::vector<SimpleParticle> ::iterator iter;
  for(iter = pts_up.begin(); iter != pts_up.end();)   {
    if ( !(iter)->isAlive() ){
      iter = pts_up.erase(iter);
    }else{
      (iter)->update();
      ++ iter;
    } 
  }

  std::vector<SimpleParticle> ::iterator iter2;
  for(iter2 = pts_down.begin(); iter2 != pts_down.end();)   {
    if ( !(iter2)->isAlive() ){
      iter2 = pts_down.erase(iter2);
    }else{
      (iter2)->update();
      ++ iter2;
    } 
  }
}

void AnimatedLine::draw(){
  path.clear();
  path.setColor(color);
  path.setFilled(true);
  path.setPolyWindingMode(OF_POLY_WINDING_POSITIVE);
  std::vector<SimpleParticle> ::iterator iter0;
  for(iter0 = pts_up.begin(); iter0 != pts_up.end();++iter0) {
    path.lineTo((iter0)->getPosition());
  }
  for(int i = pts_down.size(); i-- ; i>=0) {
    path.lineTo(pts_down[i].getPosition());
  }
  
  path.close();

  ofPushMatrix();
  ofPushStyle();
  ofSetColor(color);
  ofFill();
  path.draw();
  ofPopStyle();
  ofPopMatrix();
}

void AnimatedLine::addVertex(ofPoint pt_in, ofPoint vitesse, int width){
  ofPoint pt_up;
  ofPoint pt_down;

  pt_up.set(pt_in.x, pt_in.y + width/2);
  cout << pt_up.x << " " << pt_up.y << endl;
  SimpleParticle new_particle_up(pt_up, vitesse, 0);  
  new_particle_up.setLifespan(lifespan);
  pts_up.push_back(new_particle_up);

  pt_down.set(pt_in.x, pt_in.y - width/2);
  SimpleParticle new_particle_down(pt_down, vitesse, 0);  
  new_particle_down.setLifespan(lifespan);
  pts_down.push_back(new_particle_down);
}

AnimatedLine::~AnimatedLine(){

}
