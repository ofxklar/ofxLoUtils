#ifndef ANIMATED_ANIMATED_H
#define ANIMATED_LINE_H

#pragma once

#include "ofxLoUtils.h"
#include "ofxLoSimpleParticle.h"

class AnimatedLine
{
public:
AnimatedLine();

virtual ~AnimatedLine();

void update();
void draw();


void setColor(ofColor col_in);  
void setLifespan(int life_in);
void addVertex(ofPoint pt, ofPoint vitesse, int width); 

protected:
int lifespan;
ofColor color;
ofPath path;
std::vector<SimpleParticle> pts_down, pts_up;
};

#endif
