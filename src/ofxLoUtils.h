#ifndef _OFXLOUTILS
#define _OFXLOUTILS

#include "ofMain.h"
#include "stdio.h"
#include "math.h"

//#include "ofxLoUtilsColor.h"

class MonitorRef
{
 public:
  int   pos_x;
  int   pos_y;
  float ratio_x;
  float ratio_y;
  int   height;
  int   width;
};
 
ofPoint changeRef(ofPoint xy, MonitorRef ref);
ofColor noteToColorFifth(float midi_note);
void hsv2rgb(int h, int & r,int & g, int & b );

#include "ofxLoDelayParticles.h"
#include "ofxLoAnimatedLine.h"
#include "ofxLoUtilsVertex.h"
#include "ofxLoUtilsVertexManager.h"

 
#endif
