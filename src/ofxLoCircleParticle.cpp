#include "ofxLoCircleParticle.h"

CircleParticle::CircleParticle(ofPoint pt1_in, ofPoint pt2_in, ofPoint vitesse, int size_in) : SimpleParticle()
{
  pt1 = pt1_in;
  pt2 = pt2_in;
  vit = vitesse;
  accel.set(0,0);

  size = size_in;
}

void CircleParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;

  vit += accel * step_duration / 1000.0;

  pt2 += vit * step_duration / 1000.0;
  bAlive = isAlive();
}

void CircleParticle::draw(){
  int dist;
  dist = sqrt( pow((pt2.x - pt1.x),2) + pow((pt2.y - pt1.y), 2) );
  ofPushStyle();
  ofSetColor(color);
  ofNoFill();
  ofSetLineWidth(size);
  ofCircle(pt1, dist);
  ofPopStyle();
}

CircleParticle::~CircleParticle(){
  
}
