#ifndef SIMPLE_PARTICLE_H
#define SIMPLE_PARTICLE_H

#include "ofxLoBaseParticle.h"
#include "ofMain.h"

class SimpleParticle : public BaseParticle
{
 public:
  SimpleParticle();
  SimpleParticle(ofPoint pos, ofPoint vit, int size);
  ~SimpleParticle();
  
  void update();
  void draw();
  void drawDebug();

  void set(ofPoint pos, ofPoint vit, int size);
  void setData(map<string, float> data_in);

  bool isAlive() const;
  bool isBoom();

  ofPoint getPosition() const;
  ofPoint getVit() const;
  ofPoint getAccel() const;

  void setLifespan(int life_in);
  void setPosition(ofPoint new_pos);
  void setPosition(ofPoint pt1, ofPoint pt2);
  void setVit(ofPoint vit);
  void setColor(int r, int g, int b, int alpha);
  void setColor(ofColor col);
  void setColor(int col, float sat);
  void setAccel(ofPoint accel);
  void setLatAccel(float lat_accel);

  void setRepulsiveBox(float width, float height, float accel_max, float dist);
  void setRepulsiveBox(ofRectangle box, float accel_max, float dist);
  void updateRepulsiveBox();
  
  map<string, float> data;

 protected:
  ofPoint pos;
  ofPoint vit;
  ofPoint accel;
  ofPoint lat_accel;
  float lat_accel_norm;


  int size;
  float cur_size;
  ofColor color;
  float opacity;
  bool fade_out;

  // env gen
  float atk;
  float rel;
  float sus;

  int birthdate;
  int age;
  int lifespan;
  bool bAlive;

  bool is_boom;


  int date;
  int old_date;
  int step_duration;

  // repulsive edge
  bool box_mode;
  ofRectangle box;
  float box_accel_max;
  float box_dist;

  ofPoint pt1;
  ofPoint pt2;

};

#endif
