#ifndef RECTANGLE_PARTICLE_H
#define RECTANGLE_PARTICLE_H

#include "ofxLoSimpleParticle.h"

class RectangleParticle : public SimpleParticle
{
 public:
  RectangleParticle(ofPoint position, ofPoint vit, int size, int width);
  virtual ~RectangleParticle();

  void draw();

 protected:
  int width;
  float angle;
};

#endif
