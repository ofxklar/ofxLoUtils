#ifndef BASE_PARTICLE_H
#define BASE_PARTICLE_H

#include "ofMain.h"

class BaseParticle
{
 public:
  BaseParticle();
  virtual ~BaseParticle() = 0;
  
  virtual void update() = 0;
  virtual void draw() = 0;
 protected:
};

#endif
