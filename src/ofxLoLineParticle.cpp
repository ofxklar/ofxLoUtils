#include "ofxLoLineParticle.h"

LineParticle::LineParticle(ofPoint pt1_in, ofPoint pt2_in, ofPoint vitesse, int size_in) : SimpleParticle()
{
  pt1 = pt1_in;
  pt2 = pt2_in;
  vit = vitesse;
  accel.set(0,0);

  size = size_in;
}

void LineParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;

  vit += accel * step_duration / 1000.0;

  pt1 += vit * step_duration / 1000.0;
  pt2 += vit * step_duration / 1000.0;

  bAlive = isAlive();
}

void LineParticle::draw(){
  ofPushStyle();
  ofSetColor(color);
  ofFill();
  ofSetLineWidth(size);
  ofLine(pt1, pt2);
  ofPopStyle();
}

LineParticle::~LineParticle(){
  
}
