#include "ofxLoSquareParticle.h"

SquareParticle::SquareParticle(ofPoint position, ofPoint vitesse, int size_in) : SimpleParticle(position, vitesse, size_in)
{
  accel.set(0,0);
  angle = atan(vitesse.y/vitesse.x) * 180 / 3.14;
}

void SquareParticle::draw(){

  ofPushStyle();
  ofPushMatrix();
  ofTranslate(pos.x, pos.y);
  ofRotate(angle+45);
  ofSetColor(color);
  ofFill();
  ofRect(-size/2, -size/2, size, size);
  ofNoFill();
  ofSetColor(color*0.5);
  ofRect(-size/2, -size/2, size, size);
  ofPopMatrix();
  ofPopStyle();

}

SquareParticle::~SquareParticle(){
  
}
