#include "ofxLoSimpleParticle.h"

SimpleParticle::SimpleParticle(): BaseParticle(){
  pos.set(0,0);
  vit.set(0,0);
  accel.set(0,0);

  lat_accel_norm = 0.0;
  box_mode = false;

  size = 0;
  cur_size = 0;
  color.set(255,255,255);

  lifespan = 10000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;

  fade_out = true;
  opacity = 255;

  atk = 150;
  rel = 300;
  sus = 0.7;
  
  is_boom = false;
}

SimpleParticle::SimpleParticle(ofPoint position, ofPoint vitesse, int size_in): BaseParticle(){
  pos = position;
  vit = vitesse;
  accel.set(0,0);

  lat_accel_norm = 0.0;

  size = size_in;
  color.set(255,255,255);

  lifespan = 10000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;
}

void SimpleParticle::set(ofPoint position, ofPoint vitesse, int size_in){
  pos = position;
  vit = vitesse;

  size = size_in;
}

void SimpleParticle::setData(map<string, float> data_in){
  data = data_in;
}

void SimpleParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;

  lat_accel = ofPoint(0,0,0);

  if (lat_accel_norm != 0.0){
    lat_accel = vit.getPerpendicular(ofPoint(0, 0, 1)).getNormalized() * lat_accel_norm;
  }

  if (box_mode){
    updateRepulsiveBox();
  }

  vit += (accel+lat_accel) * step_duration / 1000.0;
  pos += vit * step_duration / 1000.0;
 
  // cout << "part update " << pos.x << endl;
  bAlive = isAlive();
  if (fade_out){
    opacity = ofMap(age, 0, lifespan, 255, 0);
  }

  if (age < atk){
    cur_size = ofMap(age, 0, atk, 0, size);
  }else if( age< atk+rel){
    cur_size = ofMap(age, atk, atk+rel, size, size*sus);
  }
}

void SimpleParticle::draw(){
  ofPushStyle();
  ofSetColor(color, opacity);
  ofFill();
  ofDrawCircle(pos.x, pos.y, cur_size/2);
  ofNoFill();
  ofSetColor(color*0.5, opacity);
  // ofSetColor(0,0,0);
  ofDrawCircle(pos.x, pos.y, cur_size/2);
  ofSetColor(255,255,255);
  ofPopStyle();
}

void SimpleParticle::drawDebug(){
  draw();
  ofPushStyle();
  ofSetColor(0, 255, 0);
  ofDrawLine(pos.x, pos.y, pos.x + vit.x, pos.y + vit.y);
  ofSetColor(0, 0, 255);
  ofDrawLine(pos.x, pos.y, pos.x + accel.x, pos.y + accel.y);
  ofSetColor(0, 0, 200);
  ofDrawLine(pos.x, pos.y, pos.x + lat_accel.x, pos.y + lat_accel.y);
  ofPopStyle();
}

void SimpleParticle::setLifespan(int life_in){
  lifespan = life_in;
}

bool SimpleParticle::isBoom()
{
  return is_boom;
}

bool SimpleParticle::isAlive() const
{
  if (age < lifespan){
    return true;
  }else{
    return false;
  }
}

ofPoint SimpleParticle::getPosition() const
{
  return pos;
}

ofPoint SimpleParticle::getVit() const
{
  return vit;
}

ofPoint SimpleParticle::getAccel() const
{
  return accel;
}


void SimpleParticle::setPosition(ofPoint pt1_in, ofPoint pt2_in){
  cout << "set POSITION2" << endl;
  pt1 = pt1_in;
  pt2 = pt2_in;
}


void SimpleParticle::setPosition(ofPoint new_pos){
  pos = new_pos; 
}

void SimpleParticle::setVit(ofPoint vitesse){
  vit = vitesse; 
}

void SimpleParticle::setColor(int r, int g, int b, int alpha){
  color.set(r, g, b, alpha);
}

void SimpleParticle::setColor(ofColor col){
  color = col;
}

void SimpleParticle::setColor(int col, float sat){
  color.setHsb(col, 255, 255, sat*255);
}

void SimpleParticle::setAccel(ofPoint accel_in){
  accel = accel_in;
}

void SimpleParticle::setLatAccel(float lat_accel_norm_in){
  lat_accel_norm = lat_accel_norm_in;
}

void SimpleParticle::setRepulsiveBox(float width_in, float height_in, float accel_max_in, float dist_in){
  box_mode = true;
  box.x = 0;
  box.y = 0;
  box.width = width_in;
  box.height = height_in;
  box_accel_max = accel_max_in;
  box_dist = dist_in;
}

void SimpleParticle::setRepulsiveBox(ofRectangle box_in, float accel_max_in, float dist_in){
  box_mode = true;
  box = box_in;
  box_accel_max = accel_max_in;
  box_dist = dist_in;
}

void SimpleParticle::updateRepulsiveBox(){
  float dist_x_max = box.x + box.width - pos.x;
  float dist_y_max = box.y + box.height - pos.y;
  float dist_x_min = pos.x - box.x;
  float dist_y_min = pos.y - box.y;

  if (dist_x_max < box_dist){
    accel.x = -ofMap(dist_x_max, 0, box_dist, box_accel_max, 0);
  }else if (dist_x_min < box_dist){
    accel.x = ofMap(dist_x_min, 0, box_dist, box_accel_max, 0);
  }else{
    accel.x = 0;
  }

  if (dist_y_max < box_dist){
    accel.y = -ofMap(dist_y_max, 0, box_dist, box_accel_max, 0);
  }else if (dist_y_min < box_dist){
    accel.y = ofMap(dist_y_min, 0, box_dist, box_accel_max, 0);
  }else{
    accel.y = 0;
  }
}

SimpleParticle::~SimpleParticle(){
  
}
