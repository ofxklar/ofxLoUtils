#ifndef _OFXLOUTILSVERTEX
#define _OFXLOUTILSVERTEX

#include "ofxLoUtils.h"
//#include "ofMain.h"
//  class MonitorRef 
// {
//  public:
//   int   pos_x;
//   int   pos_y;
//   float ratio_x;
//   float ratio_y;
//   int   height;
//   int   width;
// };
 

class DraggableVertex
{
 public:
  DraggableVertex(ofPoint pos_in, int radius_in, ofColor color_in);
  virtual ~DraggableVertex();

  void mousePressed(int x, int y, int button);
  void mouseDragged(int x, int y, int button);
  void mouseReleased(int x, int y, int button);

  void setMonitorRef(MonitorRef monitor_int);
  void setColor(ofColor col_in);
  void setOpacity(float opacity_in);
  void setContour(ofColor col_cont, int line_width_in);
  void setTrack(int track_in);
  void setType(string type_in);


  bool isOver(int x, int y);

  void desactivate();
  bool isActive();
  ofPoint getPosition();
  int getSize();
  ofColor getColor();  
  int getTrack();  
  float getRadius();  

  void draw();
 
 protected:
  ofPoint pt;
  bool 	bBeingDragged;
  bool 	bOver;
  bool bActive;
  bool bFill;
  int line_width;
  float radius; 
  MonitorRef monitor;
  bool b_monitor;
  ofColor color;
  ofColor color_contour;
  int track;
  string type;
  float opacity;
};

#endif
