#ifndef LINE_PARTICLE_H
#define LINE_PARTICLE_H

#include "ofxLoSimpleParticle.h"

class LineParticle : public SimpleParticle
{
 public:
  LineParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size);
  virtual ~LineParticle();
  
  void update();
  void draw();

 protected:
  ofPoint pt1;
  ofPoint pt2;
};

#endif
