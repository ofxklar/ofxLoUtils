#ifndef _OFXLOUTILSVERTEXMANAGER
#define _OFXLOUTILSVERTEXMANAGER

#include "ofxLoUtils.h"

class DraggableVertex;

class VertexManager
{
 public:
  VertexManager();
  virtual ~VertexManager();
  
  void update();
  void draw();

  void addVertex(ofPoint pos, int radius, ofColor color);
  void addVertex(DraggableVertex vertex);

  bool isOver(int id, int x, int y);

  void removeVertex(int id);
  void desactivateVertex(int id);

  void mousePressed(ofMouseEventArgs &e);
  void mouseDragged(ofMouseEventArgs &e);
  void mouseReleased(ofMouseEventArgs &e);

  void setVertexOpacity(int id, float opacity);

  DraggableVertex getVertex(int id);
  ofPoint getVertexPosition(int id);
  int getVertexSize(int id);
  ofColor getVertexColor(int id);
  int getLength();

 protected:
    std::vector<DraggableVertex> verticesList;

};
#endif
