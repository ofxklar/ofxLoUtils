#ifndef DELAY_PARTICLES_H
#define DELAY_PARTICLES_H

#pragma once

#include "ofxLoUtils.h"

#include "ofxLoSimpleParticle.h"
#include "ofxLoLineParticle.h"
#include "ofxLoCircleParticle.h"
#include "ofxLoSquareParticle.h"
#include "ofxLoRectangleParticle.h"
//#include "utils.h"

class DelayParticles
{
 public:
  DelayParticles();
  virtual ~DelayParticles();

  void update();
  void draw();

  void setLifespan(int life_in);
  void addParticle(ofPoint pos, ofPoint vit, int size, int col, float sat, ofPoint accel);
  void addLineParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size, ofColor col, ofPoint accel);
  void addCircleParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, int size, ofColor col, ofPoint accel);
  void addCircleParticle(CircleParticle new_particle);
  void addSquareParticle(ofPoint pos, ofPoint vit, int size, int col, float sat, ofPoint accel);
  void addRectangleParticle(ofPoint pos, ofPoint vit, int size, int width, int col, float sat, ofPoint accel);
  void addRectangleParticle(RectangleParticle new_particle);

 protected:
  int lifespan;
  std::vector<SimpleParticle> particleList;
  std::vector<LineParticle> lineParticleList;
  std::vector<CircleParticle> circleParticleList;
  std::vector<SquareParticle> squareParticleList;
  std::vector<RectangleParticle> rectangleParticleList;

};

#endif
